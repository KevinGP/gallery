import React, { Component } from 'react';
import Menu from './components/Menu';
import Albums from './components/albums'
import Photos from './components/photos'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

class RoutesApp extends Component {

  render() {
    return (
      <Router>
        <Menu />
        <Switch>
            <Route path="/" exact component={Albums} />
            <Route path="/albums" exact component={Albums} />
            <Route path="/photos" exact component={Photos} />
        </Switch>
      </Router>
    )
  }
}

export default RoutesApp;
