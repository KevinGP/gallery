import { useState, useEffect } from 'react';
import { projectStorage, projectFirestore, timestamp } from '../firebase/config';

const useStorage = (file, object, filename, collection) => {
  const [error, setError] = useState(null);
  const [url, setUrl] = useState(null);
  const header = object.header;
  const meta = object.meta;
  const target = object.target;
  const element = object;
  let idFile = object.id;

  useEffect(() => {

    if (target === 'Add' && filename != null) {
      // references
      const storageRef = projectStorage.ref(file.name);
      const collectionRef = projectFirestore.collection(collection);
      storageRef.put(file).on('state_changed', (snap) => {
        let percentage = (snap.bytesTransferred / snap.totalBytes) * 100;
      }, (err) => {
        setError(err);
      }, async () => {
        const url = await storageRef.getDownloadURL();
        const createdAt = timestamp();
        await collectionRef.add({ url, createdAt, header, meta, filename });
        setUrl(url);
      });
    }else if (target === 'Edit') {
      // Add a new document in collection "cities"
      const createdAt = timestamp();
      const url = element.url;
      const filename = element.filename;
      projectFirestore.collection('images').doc(idFile).set( {
        url: url, createdAt: createdAt, header: header, meta: meta, filename: filename
      } )
      .then(function() {
        console.log("Document successfully written!");
      })
      .catch(function(error) {
        console.error("Error writing document: ", error);
      });
    }else if (target === 'Delete' ) {
      console.log(object.filename);
      projectStorage.ref(object.filename).delete();
      
      projectFirestore.collection('images').doc(idFile).delete().then(function() {
        console.log("Document successfully deleted!");
      }).catch(function(error) {
          console.error("Error removing document: ", error);
      });
    }
    
  }, [file]);

  return { url, error };
}

export default useStorage;