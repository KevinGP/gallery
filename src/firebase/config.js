import * as firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';

var firebaseConfig = {
    apiKey: "AIzaSyBrm_CWTaGtB8-fTi-RkRprERuli21enpc",
    authDomain: "gallery-fd074.firebaseapp.com",
    databaseURL: "https://gallery-fd074.firebaseio.com",
    projectId: "gallery-fd074",
    storageBucket: "gallery-fd074.appspot.com",
    messagingSenderId: "140994765217",
    appId: "1:140994765217:web:ab40ffa45547090624abe0",
    measurementId: "G-4JDH0KX4W5"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// firebase.analytics();

const projectStorage = firebase.storage();
const projectFirestore = firebase.firestore();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;

export { projectStorage, projectFirestore, timestamp };