import React, {useState} from 'react'
import { Button, Header, Modal, Icon, Form, Image } from 'semantic-ui-react'
import FormAlbum from './form'

const element = {
    name: '',
    description: '',
    tags: '',
    photos: ''
}

const MdAlbums = ( item ) => {

  const [open, setOpen] = useState(false)
  const [elements, setElements] = useState(element)

  const handleElement = (type, value) => {
    let newElements = elements;
  
    switch (type) {
      case 'name':
        newElements.header = value;
        break;
      case 'description':
        newElements.meta = value;
        break;
      case 'tags':
        newElements.tags = value;
        break;
      case 'photos':
        newElements.tags = value;
        break;
      default:
        break;
    }
    setElements(newElements)
  }
  
  const addModal = () => {
    return (
      <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button floated='right' size='medium' icon='add'></Button>}
      >
      <Modal.Header>Add Photo</Modal.Header>
      <Modal.Content image>
          <Modal.Description>
            <FormAlbum setElements={handleElement} file='undefined' />
          </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
          <Button color='red' onClick={() => setOpen(false)}>
              Cancelar
          </Button>
          <Button
          content="Guardar"
          labelPosition='right'
          icon='save'
          onClick={() => {handleSave()}}
          positive
          />
      </Modal.Actions>
      </Modal>
    )
  }

  const imageModal = () => {
    return(
      <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Image src={item.url}></Image>}
      >
      <Modal.Content className='newContent'>
        <div className='newContent'>
          <Image src={item.url} size='large'></Image>
        </div>
        
      </Modal.Content>
      </Modal>
    )
  }

  const editModal = () => {
    return (
      <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button className='create-new-btn' icon='edit'></Button>}
      >
      <Modal.Header>Edit Photo</Modal.Header>
      <Modal.Content image>
          <Modal.Description>
            <FormAlbum setElements={handleElement} file={item.item} />
            <br></br>
          </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
          <Button color='red' onClick={() => setOpen(false)}>
              Cancelar
          </Button>
          <Button
          content="Guardar"
          labelPosition='right'
          icon='save'
          onClick={() => {handleEdit()}}
          positive
          />
      </Modal.Actions>
      </Modal>
    )
  }

  const deleteModal = () => {
    return (
      <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button icon='trash alternate'></Button>}
      >
      <Header icon>
        <Icon name='trash alternate' />
        Delete file
      </Header>
      <Modal.Content>
        <p>
          Are you sure you want to delete this file?
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='green' inverted onClick={() => setOpen(false)}>
           Cancel
        </Button>
        <Button color='red' inverted onClick={() => {handleDelete(item.idFile)}}>
          <Icon name='trash alternate' /> Yes
        </Button>
      </Modal.Actions>
      </Modal>
    )
  }
  
  let handleSave = () => {
    const object = element;
    if (object.file == null || object.header == '' || object.meta == '') {
      console.log('error');
    }else{
      object.target = item.boton;
      item.addElement(object);
      setOpen(false);
    }
  }

  let handleDelete = () => {
    const object = element;
    object.id = item.item.id;
    object.target = item.boton;
    object.filename = item.item.filename;
    item.addElement(object);
    setOpen(false);
  }

  let handleEdit = () => {
    const object = element;
    object.id = item.item.id;
    object.target = item.boton;
    item.addElement(object);
    setOpen(false);
  }

  if (item.boton == "Add") {
    return(addModal())
  }else if (item.boton == "Image") {
    return(imageModal())
  }else if (item.boton == "Edit") {
    return(editModal())
  }else{
    return(deleteModal())
  }
}

export default MdAlbums