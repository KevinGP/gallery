import React, { Component } from 'react'
import { PropTypes } from 'prop-types'
import { Form } from 'semantic-ui-react'

class FormAlbum extends Component {
  constructor(props){
    super(props)

    this.state = {
        name: '',
        description: '',
        tags: '',
        photos: ''
    }
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;

    if (this.state.hasOwnProperty(name)) {
      this.setState({ [name]: value });
      this.props.setElements(name, value);
    }
  }

  render() {
    return (
        <Form>
            <Form.Input fluid name='name' label='Name' placeholder='Album Name' onChange={this.handleChange} />
            <Form.Input fluid name='description' label='Description' placeholder='Tell more about the album' onChange={this.handleChange}/>
            <Form.Input fluid name='tags' label='Tags' placeholder='Tags' onChange={this.handleChange} />
            <Form.Input fluid name='photos' label='Photos' onChange={this.handleChange} />
        </Form>
    )
  }
}

export default FormAlbum