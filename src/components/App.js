import React, { Component, Fragment } from 'react';
import Routes from '../routes'
import 'semantic-ui-css/semantic.min.css'
import '../main.css'

class App extends Component {

  render() {
    return (
      <Fragment>
        <section className="app">
          <Routes />
        </section>
      </Fragment>
    );
  }

}

export default App;
