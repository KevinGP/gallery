import React, {useState, useEffect} from 'react';
import { Dropdown, Menu, Divider, Segment, Grid, Image, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom'

const MenuApp = () => {
  const [item, setItem] = useState('albums');

  useEffect(() => {
    // code to run on component mount
    setItem(localStorage.getItem('menuItem'));
  }, [])

  function handleLink (menuItem) {
    localStorage.setItem('menuItem', menuItem)
    setItem(menuItem);
  }
  
    const renderUser = () => {
      return (
        <span>
          <span className="menu-user--name">user</span>
          <Image avatar src={require('../imagenes/guest-user.jpg')} />
        </span>
      )
    }
  
    return (
      <Segment>
        <Grid>
          <Grid.Column width={2} verticalAlign='middle' textAlign='center'>
            <Icon name='camera retro' size='big' />
          </Grid.Column>
          <Grid.Column width={11}>
            <Menu secondary size="large">
              <Link to="/albums">
                <Menu.Item name="Albums" active={item === 'albums'} onClick={() => handleLink('albums')}/>
              </Link>
              <Link to="/photos">
                <Menu.Item name="Photos" active={item === 'photos'} onClick={() => handleLink('photos')} />
              </Link>
            </Menu>
          </Grid.Column>
          <Grid.Column width={2}>
            <div className="menu-user">
              <Dropdown item trigger={renderUser()} float="right">
                <Dropdown.Menu>
                  <Dropdown.Item name="singout" >Mi cuenta</Dropdown.Item>
                  <Divider />
                  <Dropdown.Item name="singout">Cerrar Sesión</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </Grid.Column>
        </Grid>
      </Segment>
    );
  }
  
  export default MenuApp;