import React, { Component } from 'react'
import { PropTypes } from 'prop-types'
import { Form } from 'semantic-ui-react'

const types = ['image/png', 'image/jpeg'];

class FormPhotos extends Component {
  constructor(props){
    super(props)

    this.state = {
        name: '',
        description: '',
        url: '',
        file: null,
        error: null
    }
    const propFile = this.props.file;
    if (this.props.file != 'undefined') {
      this.setState({name: propFile.header, description: propFile.meta})
    }
  }

  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;

    if (this.state.hasOwnProperty(name)) {
      this.setState({ [name]: value });
      this.props.setElements(name, value);
    }
  }

  handleFile = (e) => {
    let selected = e.target.files[0];

    if (selected && types.includes(selected.type)) {
      this.setState({file: selected});
      this.setState({error: ''});
    } else {
      this.setState({file: null});
      this.setState({error: 'Please select an image file (png or jpg)'});
    }
    
  };

  render() {
    return (
        <Form>
            <Form.Input fluid name='name' label='Name' placeholder='Album Name' defaultValue={this.props.file.header} onChange={this.handleChange} />
            <Form.Input fluid name='description' label='Description' placeholder='Tell more about the album' defaultValue={this.props.file.meta} onChange={this.handleChange}/>
        </Form>
    )
  }
}

export default FormPhotos