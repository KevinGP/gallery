import React, {useState} from 'react'
import { Button, Header, Modal, Icon, Form, Image } from 'semantic-ui-react'
import FormPhoto from './form'

const element = {
  header: '',
  meta: '',
  file: null,
  target: null,
  id: null,
  filename: null
}

const MdPhotos = ( item ) => {

  const [open, setOpen] = useState(false)
  const [elements, setElements] = useState(element)

  const handleElement = (type, value) => {
    let newElements = elements;
  
    switch (type) {
      case 'name':
        newElements.header = value;
        break;
      case 'description':
        newElements.meta = value;
        break;
      case 'url':
        newElements.image = value;
        break;
      default:
        break;
    }
    setElements(newElements)
  }

  // 
  // INPUT FILE
  const [error, setError] = useState(null);
  let newElements = elements;

  const types = ['image/png', 'image/jpeg'];

  const handleChange = (e) => {
    let selected = e.target.files[0];

    if (selected && types.includes(selected.type)) {
      newElements.file = selected;
      // setFile(selected);
      // setError('');
    } else {
      // setFile(null);
      setError('Please select an image file (png or jpg)');
    }
    setElements(newElements);
  };
  
  const addModal = () => {
    return (
      <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button floated='right' size='medium' icon='add'></Button>}
      >
      <Modal.Header>Add Photo</Modal.Header>
      <Modal.Content image>
          <Modal.Description>
            <FormPhoto setElements={handleElement} file='undefined' />
            <br></br>
            <label>
              <input type="file" onChange={handleChange}></input>
            </label>
            <div className="output">
              { error && <div className="error">{ error }</div>}
            </div>
          </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
          <Button color='red' onClick={() => setOpen(false)}>
              Cancelar
          </Button>
          <Button
          content="Guardar"
          labelPosition='right'
          icon='save'
          onClick={() => {handleSave()}}
          positive
          />
      </Modal.Actions>
      </Modal>
    )
  }

  const imageModal = () => {
    return(
      <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Image src={item.url}></Image>}
      >
      <Modal.Content className='newContent'>
        <div className='newContent'>
          <Image src={item.url} size='large'></Image>
        </div>
        
      </Modal.Content>
      </Modal>
    )
  }

  const editModal = () => {
    return (
      <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button className='create-new-btn' icon='edit'></Button>}
      >
      <Modal.Header>Edit Photo</Modal.Header>
      <Modal.Content image>
          <Modal.Description>
            <div className="output">
              { error && <div className="error">{ error }</div>}
            </div>
            <FormPhoto setElements={handleElement} file={item.item} />
            <br></br>
          </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
          <Button color='red' onClick={() => setOpen(false)}>
              Cancelar
          </Button>
          <Button
          content="Guardar"
          labelPosition='right'
          icon='save'
          onClick={() => {handleEdit()}}
          positive
          />
      </Modal.Actions>
      </Modal>
    )
  }

  const deleteModal = () => {
    return (
      <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button icon='trash alternate'></Button>}
      >
      <Header icon>
        <Icon name='trash alternate' />
        Delete file
      </Header>
      <Modal.Content>
        <p>
          Are you sure you want to delete this file?
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='green' inverted onClick={() => setOpen(false)}>
           Cancel
        </Button>
        <Button color='red' inverted onClick={() => {handleDelete(item.idFile)}}>
          <Icon name='trash alternate' /> Yes
        </Button>
      </Modal.Actions>
      </Modal>
    )
  }
  
  let handleSave = () => {
    const object = element;
    if (object.file == null || object.header == '' || object.meta == '') {
      setError('Please fill all the fields')
    }else{
      object.target = item.boton;
      item.addElement(object);
      setOpen(false);
    }
  }

  let handleDelete = () => {
    const object = element;
    object.id = item.item.id;
    object.target = item.boton;
    object.filename = item.item.filename;
    item.addElement(object);
    setOpen(false);
  }

  let handleEdit = () => {
    const object = element;
    object.id = item.item.id;
    object.target = item.boton;
    item.addElement(object);
    setOpen(false);
  }

  if (item.boton == "Add") {
    return(addModal())
  }else if (item.boton == "Image") {
    return(imageModal())
  }else if (item.boton == "Edit") {
    return(editModal())
  }else{
    return(deleteModal())
  }
}

export default MdPhotos