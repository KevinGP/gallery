import React, {useState} from 'react';
import { Button, Card, Grid, Segment } from 'semantic-ui-react'
import Modal from './modal'
import useFirestore from '../../hooks/useFirestore';
import ProgressBar from './progressBar'

const Photos = () => {
  const { docs } = useFirestore('images');
  const [file, setFile] = useState(null);
  const [object, setObject] = useState(null);
  const [target, setTarget] = useState(null);

  const pushPhoto = (element) => {
    setObject(element);
    setTarget(element.target);
    setFile(element.file);
  }
  
  let photoCard = docs.map((item, i) => {
    return (
      <Card>
        <Card.Content>
          <Modal boton='Image' url={item.url} />
          
          <Card.Header><br />{item.header}</Card.Header>
          <Card.Meta>{item.meta}</Card.Meta>
        </Card.Content>
        <Card.Content extra> 
          <Button.Group fluid>
            <Modal boton='Edit' item={item} addElement={pushPhoto} />
            <Modal boton='Delete' item={item} addElement={pushPhoto} />
          </Button.Group>
        </Card.Content>
      </Card>
    );
  });
  
  return (
    <div className='divs'>
      <Segment>
        <Grid columns='equal'>
          <Grid.Column verticalAlign='middle'>
            {docs.length} Photo(s) Total
          </Grid.Column>
          <Grid.Column >
            <Modal boton="Add" addElement={pushPhoto} />
          </Grid.Column>
        </Grid>
        </Segment>
        { target && <ProgressBar file={file} setFile={setFile} object={object} /> } 
      <Card.Group itemsPerRow={6}>
        {photoCard}
      </Card.Group>

    </div>
  );
}
  
  export default Photos;