import React, { useEffect } from 'react';
import useStorage from '../../hooks/useStorage';
import { motion } from 'framer-motion';

const ProgressBar = ({ file, setFile, object }) => {
  console.log('entro');
  let filename = null;
  if (file != null) {
    filename = file.name;
  }
  const { progress, url } = useStorage(file, object, filename);

  useEffect(() => {
    if (url) {
      setFile(null);
    }
  }, [url, setFile]);

  return (
    <motion.div className="progress-bar"
      initial={{ width: 0 }}
      animate={{ width: progress + '%' }}
    ></motion.div>
  );
} 

export default ProgressBar;